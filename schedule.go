package main

import (
	"fmt"
	"google.golang.org/api/calendar/v3"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type SkateLocation struct {
	Name        string `yaml:"name"`
	Active      bool   `yaml:"active"`
	Location    string `yaml:"location"`
	Description string `yaml:"description"`
}

type SkateSession struct {
	Location  string `yaml:"rink"`
	StartTime string `yaml:"start_time"`
	EndTime   string `yaml:"end_time"`
}

type SkateSessionFull struct {
	SkateSession  *SkateSession
	SkateLocation *SkateLocation
	Day           string
}

type OpenSkate struct {
	Monday    []SkateSession `yaml:"monday"`
	Tuesday   []SkateSession `yaml:"tuesday"`
	Wednesday []SkateSession `yaml:"wednesday"`
	Thursday  []SkateSession `yaml:"thursday"`
	Friday    []SkateSession `yaml:"friday"`
	Saturday  []SkateSession `yaml:"saturday"`
	Sunday    []SkateSession `yaml:"sunday"`
}

type SkateSchedule struct {
	SkateLocations []SkateLocation `yaml:"rinks"`
	OpenSkates     OpenSkate       `yaml:"open_skates"`
}

func (schedule *SkateSchedule) GetSkateLocationByName(name string) *SkateLocation {
	for _, v := range schedule.SkateLocations {
		if v.Name == name {
			return &v
		}
	}
	return nil
}

type SkateScheduleFull struct {
	SkateSchedule     *SkateSchedule
	SkateSessionsFull []SkateSessionFull
}

func NewFullSkateSchedule(schedule *SkateSchedule) *SkateScheduleFull {
	r := SkateScheduleFull{
		SkateSchedule: schedule,
	}

	for _, session := range schedule.OpenSkates.Monday {
		fullSession := SkateSessionFull{
			SkateSession:  &session,
			SkateLocation: schedule.GetSkateLocationByName(session.Location),
			Day:           "MO",
		}
		r.SkateSessionsFull = append(r.SkateSessionsFull, fullSession)
	}
	for _, session := range schedule.OpenSkates.Tuesday {
		fullSession := SkateSessionFull{
			SkateSession:  &session,
			SkateLocation: schedule.GetSkateLocationByName(session.Location),
			Day:           "TU",
		}
		r.SkateSessionsFull = append(r.SkateSessionsFull, fullSession)
	}
	for _, session := range schedule.OpenSkates.Wednesday {
		fullSession := SkateSessionFull{
			SkateSession:  &session,
			SkateLocation: schedule.GetSkateLocationByName(session.Location),
			Day:           "WE",
		}
		r.SkateSessionsFull = append(r.SkateSessionsFull, fullSession)
	}
	for _, session := range schedule.OpenSkates.Thursday {
		fullSession := SkateSessionFull{
			SkateSession:  &session,
			SkateLocation: schedule.GetSkateLocationByName(session.Location),
			Day:           "TH",
		}
		r.SkateSessionsFull = append(r.SkateSessionsFull, fullSession)
	}
	for _, session := range schedule.OpenSkates.Friday {
		fullSession := SkateSessionFull{
			SkateSession:  &session,
			SkateLocation: schedule.GetSkateLocationByName(session.Location),
			Day:           "FR",
		}
		r.SkateSessionsFull = append(r.SkateSessionsFull, fullSession)
	}
	for _, session := range schedule.OpenSkates.Saturday {
		fullSession := SkateSessionFull{
			SkateSession:  &session,
			SkateLocation: schedule.GetSkateLocationByName(session.Location),
			Day:           "SA",
		}
		r.SkateSessionsFull = append(r.SkateSessionsFull, fullSession)
	}
	for _, session := range schedule.OpenSkates.Sunday {
		fullSession := SkateSessionFull{
			SkateSession:  &session,
			SkateLocation: schedule.GetSkateLocationByName(session.Location),
			Day:           "SU",
		}
		r.SkateSessionsFull = append(r.SkateSessionsFull, fullSession)
	}


	return &r
}

func (schedule *SkateScheduleFull) ToGoogleEvents() []calendar.Event {
	results := []calendar.Event{}
	for _, sess := range schedule.SkateSessionsFull {
		results = append(results, sess.ToGoogleEvent())
	}
	return results
}

func (ss *SkateSessionFull) ToGoogleEvent() calendar.Event {
	return calendar.Event{
		Summary:     fmt.Sprintf("[%v] Open Skate", ss.SkateLocation.Name),
		Start:       &calendar.EventDateTime{DateTime: fmt.Sprintf("2018-06-05T%v:00", ss.SkateSession.StartTime), TimeZone: "America/New_York"},
		End:         &calendar.EventDateTime{DateTime: fmt.Sprintf("2018-06-05T%v:00", ss.SkateSession.EndTime), TimeZone: "America/New_York"},
		Location:    ss.SkateLocation.Location,
		Description: ss.SkateLocation.Description,
		Recurrence:  []string{fmt.Sprintf("RRULE:FREQ=WEEKLY;WKST=SU;BYDAY=%v", ss.Day)},
	}
}

func LoadScheduleFromFile(filepath string) (*SkateSchedule, error) {
	bytes, err := ioutil.ReadFile(filepath)

	if err != nil {
		return nil, err
	}

	schedule := SkateSchedule{}

	if err := yaml.Unmarshal(bytes, &schedule); err != nil {
		return nil, err
	}

	return &schedule, nil
}
