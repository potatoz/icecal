package main

import "testing"

func TestLoadScheduleFromFileIsSuccessfulWithValidSchedule(t *testing.T) {
	schedule, e := LoadScheduleFromFile("schedule.test.yml")
	if schedule == nil {
		t.Errorf("%v", e)
	}
	if e != nil {
		t.Errorf("%v", e)
	}
}

func TestLoadScheduleFromFileResultHasSkateLocations(t *testing.T) {
	schedule, _ := LoadScheduleFromFile("schedule.test.yml")
	if schedule == nil {
		t.Errorf("shedule is nil")
	}
	if len(schedule.SkateLocations) != 2 {
		t.Errorf("expected 2 SkateLocations, but found %v", len(schedule.SkateLocations))
	}
}

func TestLoadScheduleFromFileResultHasMondayOpenSkates(t *testing.T) {
	schedule, _ := LoadScheduleFromFile("schedule.test.yml")
	if schedule == nil {
		t.Errorf("schedule is nil")
	}
	if len(schedule.OpenSkates.Monday) != 3 {
		t.Errorf("expected 3 OpenSkates on Monday, but found %v", len(schedule.OpenSkates.Monday))
	}
}



func TestNewFullSkateSchedule(t *testing.T) {
	schedule, _ := LoadScheduleFromFile("schedule.test.yml")
	if schedule == nil {
		t.Errorf("schedule is nil")
	}

	skateSchedule := NewFullSkateSchedule(schedule)
	if skateSchedule == nil {
		t.Errorf("schedule is nil")
	}
}