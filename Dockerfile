FROM golang:latest as base

RUN go get -d -u github.com/golang/dep \
    && cd $(go env GOPATH)/src/github.com/golang/dep \
    && DEP_LATEST=$(git describe --abbrev=0 --tags) \
    && git checkout $DEP_LATEST \
    && go install -ldflags="-X main.version=$DEP_LATEST" ./cmd/dep

WORKDIR $GOPATH/src/gitlab.com/potatoz/icecal
COPY *.go Gopkg.toml Gopkg.lock schedule.*.yml ./
RUN dep ensure


FROM golang:latest as build

WORKDIR $GOPATH/src/gitlab.com/potatoz/icecal
COPY --from=base $GOPATH/src/gitlab.com/potatoz/icecal .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .


FROM ubuntu as run

WORKDIR /app
COPY --from=build $GOPATH/src/gitlab.com/potatoz/icecal/app .
CMD ["/app/app"]